<?php
require_once "/opt/lampp/htdocs/searchflights/businesslogic/flights.business.php";

class FlightsModel{

	/*=============================================
	SAVE FLIGHT
	=============================================*/

	static public function mdlSaveFlights($table, $DepartureDate, $DepartureStation, $ArrivalStation){

		$response = FlightBusinessSave::busFlightSave($table, $DepartureDate, $DepartureStation, $ArrivalStation);

		return $response;		

	}

	/*=============================================
	Search Flights
	=============================================*/

	static public function mdlSearchFlights($departure, $arrival, $dateFlight){

		$response = FlightBusinessSearch::busFlightSearch($departure, $arrival, $dateFlight);

		return $response;		

	}

}
