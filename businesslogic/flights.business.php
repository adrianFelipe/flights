<?php

require_once "/opt/lampp/htdocs/searchflights/data/flights.data.php";

class FlightBusinessSave{

	/*=============================================
	Save Flights
	=============================================*/

	static public function busFlightSave($table, $DepartureDate, $DepartureStation, $ArrivalStation){

		$response = FlightsData::datSaveFlights($table, $DepartureDate, $DepartureStation, $ArrivalStation);
		return $response;

	}

}


class FlightBusinessSearch{

	/*=============================================
	Search Flights
	=============================================*/

	static public function busFlightSearch($departure, $arrival, $dateFlight){

		$response = FlightsData::datSearchFlights($departure, $arrival, $dateFlight);
		return $response;

	}

}

