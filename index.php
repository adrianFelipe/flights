<?php

require_once "functions/functions.php";
require_once "controllers/template.controller.php";
require_once "controllers/flights.controller.php";
require_once "businesslogic/flights.business.php";
require_once "data/flights.data.php";
require_once "models/flights.model.php";


set_error_handler("on_error");
$template = new TemplateController();
$template -> ctrTemplate();
